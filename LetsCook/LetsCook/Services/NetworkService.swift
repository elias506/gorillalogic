//
//  NetworkService.swift
//  LetsCook
//
//  Created by Elias Esquivel on 3/16/20.
//  Copyright © 2020 Elias Esquivel. All rights reserved.
//

import UIKit

fileprivate enum EndPoints: String {
    case allRecipes = "http://gl-endpoint.herokuapp.com/recipes"
    case oneRecipe = "http://gl-endpoint.herokuapp.com/recipes/%d"
}

fileprivate struct URLsFactory {
    var allRecipes: URL? {
        URL(string: EndPoints.allRecipes.rawValue)
    }
    
    func specificRecipe(_ identifier: Int) -> URL? {
        let stringURL = String(format: EndPoints.oneRecipe.rawValue, identifier)
        return URL(string: stringURL)
    }
}

class NetworkService {
    private let session = URLSession.shared
    
    func fetchAll(completion: @escaping RecipesFetchable.recipesCompletion) {
        guard let url = URLsFactory().allRecipes else {
            completion(nil)
            return
        }
        
        let task = session.dataTask(with: url) { data, response, error in
            guard let data = data else {
                completion(nil)
                return
            }

            do {
                let decoder = JSONDecoder()
                let recipes = try decoder.decode([Recipe].self, from: data)
                completion(recipes)
            } catch {
                completion(nil)
            }
        }

        task.resume()
    }
    
    func fetchRecipe(identifier: Int, completion: @escaping RecipesFetchable.recipeCompletion) {
        guard let url = URLsFactory().specificRecipe(identifier) else {
            completion(nil)
            return
        }
        
        let task = session.dataTask(with: url) { data, response, error in
            guard let data = data else {
                completion(nil)
                return
            }

            do {
                let decoder = JSONDecoder()
                let recipe = try decoder.decode(Recipe.self, from: data)
                completion(recipe)
            } catch {
                completion(nil)
            }
        }

        task.resume()
    }
    
    func getImage(from url: URL, completion: @escaping (_ result: UIImage?) -> Void) {
        let task = session.dataTask(with: url) { data, response, error in
            guard let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data) else {
                    completion(nil)
                    return
                }
            
            completion(image)
            
            
            }
         task.resume()
    }
}
