//
//  ImagesAPI.swift
//  LetsCook
//
//  Created by Elias Esquivel on 3/16/20.
//  Copyright © 2020 Elias Esquivel. All rights reserved.
//

import UIKit

class ImagesAPI {
    func getImage(from url: URL, completion: @escaping (_ result: UIImage?) -> Void) {
        NetworkService().getImage(from: url, completion: completion)
    }
}
