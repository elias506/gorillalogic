//
//  Recipe.swift
//  LetsCook
//
//  Created by Elias Esquivel on 3/16/20.
//  Copyright © 2020 Elias Esquivel. All rights reserved.
//

import Foundation

struct Recipe: Codable {
    let id: Int
    let title: String
    let rating: Int?
    let image: String?
    let instructions: String?
}
