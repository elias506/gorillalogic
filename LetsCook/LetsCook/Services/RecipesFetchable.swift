//
//  RecipesFetchable.swift
//  LetsCook
//
//  Created by Elias Esquivel on 3/16/20.
//  Copyright © 2020 Elias Esquivel. All rights reserved.
//

import Foundation

protocol RecipesFetchable {
    typealias recipesCompletion = ([Recipe]?) -> Void
    typealias recipeCompletion = (Recipe?) -> Void
    
    func fetchAllRecipes(completion: @escaping recipesCompletion)
    func fetchRecipe(_ identifier: Int, completion: @escaping recipeCompletion)
}
