//
//  RecipesAPI.swift
//  LetsCook
//
//  Created by Elias Esquivel on 3/16/20.
//  Copyright © 2020 Elias Esquivel. All rights reserved.
//

import UIKit

public class RecipesAPI: RecipesFetchable {
    func fetchAllRecipes(completion: @escaping recipesCompletion) {
        NetworkService().fetchAll(completion: completion)
    }
    
    func fetchRecipe(_ identifier: Int, completion: @escaping recipeCompletion) {
        NetworkService().fetchRecipe(identifier: identifier, completion: completion)
    }
}
