//
//  RecipeViewModel.swift
//  LetsCook
//
//  Created by Elias Esquivel on 3/16/20.
//  Copyright © 2020 Elias Esquivel. All rights reserved.
//

import UIKit

class RecipeViewModel {
    
    var image: UIImage?
    var imagePath: String?
    var text: String?
    var rating: Int = 0
    var title: String?
    
    let presentation: RecipePresentation
    
    init(presentation: RecipePresentation) {
        self.presentation = presentation
    }
    
    func loadData(completion: @escaping () -> Void) {
        RecipesAPI().fetchRecipe(presentation.identifier) { (recipe) in
            guard let recipe = recipe else {
                return
            }
            self.rating = recipe.rating ?? 0
            self.text = recipe.instructions
            self.imagePath = recipe.image
            self.title = recipe.title
            completion()
        }
    }
    
    //IMPROVEMENT:
    // MOVE THIS CODE TO A SEPARATE CLASS FOR REUSABILITY. MAYBE AN EXTENSION OF UIIMAGE.
    func loadImage(completion: @escaping () -> Void) {
        if let path = imagePath, let url = URL(string: path) {
            ImagesAPI().getImage(from: url) { (image) in
                self.image = image
                completion()
            }
        }
    }
}
