//
//  StarRatings.swift
//  LetsCook
//
//  Created by Elias Esquivel on 3/16/20.
//  Copyright © 2020 Elias Esquivel. All rights reserved.
//

import UIKit

class StarRatings: UIStackView {
    @IBOutlet var rate1: UIImageView!
    @IBOutlet var rate2: UIImageView!
    @IBOutlet var rate3: UIImageView!
    @IBOutlet var rate4: UIImageView!
    @IBOutlet var rate5: UIImageView!
    
    func update(rating: Int) {
        switch rating {
        case 1:
            rate1.isHighlighted = true
        case 2:
            rate1.isHighlighted = true
            rate2.isHighlighted = true
        case 3:
            rate1.isHighlighted = true
            rate2.isHighlighted = true
            rate3.isHighlighted = true
        case 4:
            rate1.isHighlighted = true
            rate2.isHighlighted = true
            rate3.isHighlighted = true
            rate4.isHighlighted = true
        case 5:
            rate1.isHighlighted = true
            rate2.isHighlighted = true
            rate3.isHighlighted = true
            rate4.isHighlighted = true
            rate5.isHighlighted = true
        default:
            break
        }
    }
}
