//
//  RecipeViewController.swift
//  LetsCook
//
//  Created by Elias Esquivel on 3/16/20.
//  Copyright © 2020 Elias Esquivel. All rights reserved.
//

import UIKit

class RecipeViewController: UIViewController {

    var inputModel: RecipeInputModel!
    private lazy var viewModel = RecipeViewModel(presentation: self.inputModel.presentation)
    
    @IBOutlet var starRatings: StarRatings!
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var text: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loadData()
    }
    
    // MARK: - Data -
    private func loadData() {
        viewModel.loadData {
            DispatchQueue.main.async {
                self.starRatings.update(rating: self.viewModel.rating)
                self.text.text = self.viewModel.text
                self.navigationItem.title = self.viewModel.title
            }
            
            self.viewModel.loadImage {
                DispatchQueue.main.async {
                    self.imageView.image = self.viewModel.image
                }
            }
        }
    }
    
    // MARK: - Configuration -
    private func configureView() {
        self.imageView.image = viewModel.image
        self.starRatings.update(rating: viewModel.rating)
        self.text.text = viewModel.text
    }

}
