//
//  RecipeInputModel.swift
//  LetsCook
//
//  Created by Elias Esquivel on 3/16/20.
//  Copyright © 2020 Elias Esquivel. All rights reserved.
//

import UIKit

struct RecipePresentation {
    let identifier: Int
}

class RecipeInputModel {
    let presentation: RecipePresentation
    
    init(presentation: RecipePresentation) {
        self.presentation = presentation
    }
}
