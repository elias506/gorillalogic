//
//  HomeViewModel.swift
//  LetsCook
//
//  Created by Elias Esquivel on 3/16/20.
//  Copyright © 2020 Elias Esquivel. All rights reserved.
//

import Foundation

class HomeViewModel {
    var searchPlaceholder = "Search"
    
    func loadAllRecipes(completion: @escaping RecipesFetchable.recipesCompletion) {
        RecipesAPI().fetchAllRecipes(completion: completion)
    }
    
}
