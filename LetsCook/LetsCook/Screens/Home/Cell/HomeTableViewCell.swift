//
//  HomeTableViewCell.swift
//  LetsCook
//
//  Created by Elias Esquivel on 3/16/20.
//  Copyright © 2020 Elias Esquivel. All rights reserved.
//

import UIKit

struct HomeTableViewCellPresentation {
    let text: String
}

class HomeTableViewCell: UITableViewCell {
    
    @IBOutlet var titleLable: UILabel!
    
    static var identifier: String {
        return String(describing: self)
    }
    
    static var nibName: String {
        return String(describing: self)
    }

    func configure(presentation: HomeTableViewCellPresentation) {
        titleLable.text = presentation.text
    }
    
}
