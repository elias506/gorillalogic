//
//  ViewController.swift
//  LetsCook
//
//  Created by Elias Esquivel on 3/16/20.
//  Copyright © 2020 Elias Esquivel. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    private lazy var viewModel = HomeViewModel()
    
    @IBOutlet var tableView: UITableView!
    private var data: [Recipe] = [Recipe]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        viewModel.loadAllRecipes { (recipes) in
            guard let recipes = recipes else {
                return
            }
            self.data = recipes
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    //MARK: - Data Source -
    private func item(at index: Int) -> HomeTableViewCellPresentation? {
        if let recipe = recipe(at: index) {
            return HomeTableViewCellPresentation(text: recipe.title)
        }
        
        return nil
    }
    
    private func recipe(at index: Int) -> Recipe? {
        if data.count > index {
            let item = data[index]
            return item
        }
        
        return nil
    }
    
    // MARK: - Configuration -
    private func configureView() {
        configureTableView()
    }
    
    private func configureTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: HomeTableViewCell.nibName, bundle: nil), forCellReuseIdentifier: HomeTableViewCell.identifier)
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 85.0
    }
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: HomeTableViewCell.identifier, for: indexPath)
        
        if let cell = cell as? HomeTableViewCell,
            let presentation = item(at: indexPath.row) {
            cell.configure(presentation: presentation)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let item = recipe(at: indexPath.row) {
            let presentation = RecipePresentation(identifier: item.id)
            openDetail(presentation: presentation)
        }
    }
    
    fileprivate func openDetail(presentation: RecipePresentation) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let viewController = storyboard.instantiateViewController(withIdentifier: "RecipeViewController") as? RecipeViewController {
            let inputModel = RecipeInputModel(presentation: presentation)
            viewController.inputModel = inputModel
            navigationController?.pushViewController(viewController, animated: true)
        }
    }
}

