//
//  RecipesDataSource.swift
//  LetsCook
//
//  Created by Elias Esquivel on 3/16/20.
//  Copyright © 2020 Elias Esquivel. All rights reserved.
//

import UIKit

class RecipesDataSource {
    private var tableView: UITableView!
    
    func configure(tableView: UITableView) {
        self.tableView = tableView
    }
}

// IMPROVEMENT:
// MOVE ALL TABLEVIEW CODE HERE.
